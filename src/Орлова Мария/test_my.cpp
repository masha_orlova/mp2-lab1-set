#include "tbitfield.h"
#include "tset.h"

#include <gtest.h>
TEST(NewTests, can_double_clear_bit)
{
  TBitField bf(10);

  int bitIdx = 3;

  bf.SetBit(bitIdx);
  EXPECT_NE(0, bf.GetBit(bitIdx));
  bf.ClrBit(bitIdx);
  bf.ClrBit(bitIdx);
  EXPECT_EQ(0, bf.GetBit(bitIdx));
}

TEST(NewTests, or_operator_applied_to_3_bitfields_of_non_equal_size)
{
  TBitField bf1(1), bf2(3), bf3(4), expBf(4);
  // bf1 = 1
  bf1.SetBit(0);
  // bf2 = 011
  bf2.SetBit(1);
  bf2.SetBit(2);
  // bf3 = 0001
  bf3.SetBit(3);

  // expBf = 1111
  expBf.SetBit(0);
  expBf.SetBit(1);
  expBf.SetBit(2);
  expBf.SetBit(3);

  EXPECT_EQ(expBf, bf1 | bf2 | bf3);
}

TEST(NewTests, can_combine_3_sets_of_non_equal_size)
{
  const int size1 = 5, size2 = 7;
  const int size3 = 9;
  TSet set1(size1), set2(size2), set3(size3), expSet(size3);
  // set1 = {1, 2, 4}
  set1.InsElem(1);
  set1.InsElem(2);
  set1.InsElem(4);
  // set2 = {0, 1, 2, 6}
  set2.InsElem(0);
  set2.InsElem(1);
  set2.InsElem(2);
  set2.InsElem(6);

  // set3 = {3, 4, 8}
  set3.InsElem(3);
  set3.InsElem(4);
  set3.InsElem(8);

  // expSet = {0, 1, 2, 3, 4, 6, 8}
  expSet.InsElem(0);
  expSet.InsElem(1);
  expSet.InsElem(2);
  expSet.InsElem(3);
  expSet.InsElem(4);
  expSet.InsElem(6);
  expSet.InsElem(8);

  EXPECT_EQ(expSet, set1 + set2 + set3);
}