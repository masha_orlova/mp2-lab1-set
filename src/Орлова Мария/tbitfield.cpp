// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#include <stdexcept>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>


#include "tbitfield.h"

static const int bits_in_elem = sizeof(TELEM) * 8;

TBitField::TBitField(int len)
{
	if (len < 0)
		throw std::runtime_error("Length should be positive");
	
	BitLen = len;
	MemLen = (len + bits_in_elem - 1) / bits_in_elem;
	pMem = new TELEM[MemLen];
	
	if (!pMem)
		throw std::runtime_error("Not enough memory");
	
	for (int i = 0; i < MemLen; i++)
		pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	if (!pMem)
		throw std::runtime_error("Not enough memory");
	for (int i = 0; i < MemLen; i++)
		pMem[i] = bf.pMem[i];	
}

TBitField::~TBitField()
{
	if (pMem) delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if(n < 0 || n > (BitLen - 1))
		throw std::runtime_error("Wrong index");
	return n / bits_in_elem;
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	if(n < 0 || n > (BitLen - 1))
		throw std::runtime_error("Wrong index");
	return (1 << n % bits_in_elem);
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
  return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	// Проверки на корректность n здесь не нужны - будет проверено в GetMemIndex или GetMemMask
	pMem[GetMemIndex(n)] |= GetMemMask(n);
}

void TBitField::ClrBit(const int n) // очистить бит
{
	// Проверки на корректность n здесь не нужны - будет проверено в GetMemIndex или GetMemMask
	pMem[GetMemIndex(n)] &= ~GetMemMask(n);
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	// Проверки на корректность n здесь не нужны - будет проверено в GetMemIndex или GetMemMask
	return pMem[GetMemIndex(n)] & GetMemMask(n);
}

// битовые операции
TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	if (this != &bf) {
		BitLen = bf.BitLen;
		MemLen = bf.MemLen;
		TELEM* tmp = new TELEM[MemLen];
		if (!tmp)
			throw std::runtime_error("Not enough memory");
		if (pMem) delete[] pMem;
		pMem = tmp;
		for (int i = 0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];	
	}
	return *this;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	if (BitLen != bf.BitLen) 
		return false;

	for (int i = 0; i < MemLen; i++)
		if (pMem[i] != bf.pMem[i])
			return false;
	return true;
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
  return !(*this == bf);
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	TBitField result(std::max(BitLen, bf.BitLen));
	for (int i = 0; i < std::max(MemLen, bf.MemLen); i++) {
		TELEM lhs = (i < MemLen) ? pMem[i] : 0;
		TELEM rhs = (i < bf.MemLen) ? bf.pMem[i] : 0;
		result.pMem[i] = lhs | rhs;
	}
	return result;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	TBitField result(std::max(BitLen, bf.BitLen));
	for (int i = 0; i < std::max(MemLen, bf.MemLen); i++) {
		TELEM lhs = (i < MemLen) ? pMem[i] : 0;
		TELEM rhs = (i < bf.MemLen) ? bf.pMem[i] : 0;
		result.pMem[i] = lhs & rhs;
	}
	return result;
}

TBitField TBitField::operator~(void) // отрицание
{
	// Чтобы не инвертировать незначащие биты делаем побитовую операцию.
	TBitField result(BitLen);
	for (int i = 0; i < BitLen; i++) {
		if (GetBit(i) == 0) 
			result.SetBit(i);
	}
	return result;
}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
	std::string line;
	std::getline(istr, line);
	std::istringstream stream(line);
	std::vector<char> usefull_data;
	char ch;
	while(stream >> ch) {
		if (ch == '0' || ch == '1')
			usefull_data.push_back(ch);
	}
	TBitField newBitField(usefull_data.size());
	for (unsigned int i = 0; i < usefull_data.size(); i++) {
		if (usefull_data[i] == '1') 
			newBitField.SetBit(i);
	}
	bf = newBitField;
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	for (int i = 0; i < bf.BitLen; i++)
		if (bf.GetBit(i))
			ostr << 1;
		else
			ostr << 0;
	return ostr;
}
