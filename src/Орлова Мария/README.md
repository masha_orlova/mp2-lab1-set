# Множества на основе битовых полей


## Цели и задачи

Цель данной работы — разработка структуры данных для хранения множеств с использованием битовых полей, а также освоение таких инструментов разработки программного обеспечения, как система контроля версий Git и фрэймворк для разработки автоматических тестов Google Test.

Выполнение работы предполагает решение следующих задач: 

 * Реализация класса битового поля TBitField согласно заданному интерфейсу.
 * Реализация класса множества TSet согласно заданному интерфейсу.
 * Обеспечение работоспособности тестов и примера использования.
 * Реализация нескольких простых тестов на базе Google Test.
 * Публикация исходных кодов в личном репозитории на GitHub.

Работа выполнялась на основе проекта-шаблона, содержащего следующее:

 * Интерфейсы классов битового поля и множества (h-файлы)
 * Готовый набор тестов для каждого из указанных классов
 * Пример использования класса битового поля и множества для решения задачи
 * поиска простых чисел с помощью алгоритма "Решето Эратосфена"

## Реализация класса TBitField

```C++

// Битовое поле

#include <stdexcept>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>


#include "tbitfield.h"

static const int bits_in_elem = sizeof(TELEM) * 8;

TBitField::TBitField(int len)
{
	if (len < 0)
		throw std::runtime_error("Length should be positive");
	
	BitLen = len;
	MemLen = (len + bits_in_elem - 1) / bits_in_elem;
	pMem = new TELEM[MemLen];
	
	if (!pMem)
		throw std::runtime_error("Not enough memory");
	
	for (int i = 0; i < MemLen; i++)
		pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	if (!pMem)
		throw std::runtime_error("Not enough memory");
	for (int i = 0; i < MemLen; i++)
		pMem[i] = bf.pMem[i];	
}

TBitField::~TBitField()
{
	if (pMem) delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if(n < 0 || n > (BitLen - 1))
		throw std::runtime_error("Wrong index");
	return n / bits_in_elem;
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	if(n < 0 || n > (BitLen - 1))
		throw std::runtime_error("Wrong index");
	return (1 << n % bits_in_elem);
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
  return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	// Проверки на корректность n здесь не нужны - будет проверено в GetMemIndex или GetMemMask
	pMem[GetMemIndex(n)] |= GetMemMask(n);
}

void TBitField::ClrBit(const int n) // очистить бит
{
	// Проверки на корректность n здесь не нужны - будет проверено в GetMemIndex или GetMemMask
	pMem[GetMemIndex(n)] &= ~GetMemMask(n);
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	// Проверки на корректность n здесь не нужны - будет проверено в GetMemIndex или GetMemMask
	return pMem[GetMemIndex(n)] & GetMemMask(n);
}

// битовые операции
TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	if (this != &bf) {
		BitLen = bf.BitLen;
		MemLen = bf.MemLen;
		TELEM* tmp = new TELEM[MemLen];
		if (!tmp)
			throw std::runtime_error("Not enough memory");
		if (pMem) delete[] pMem;
		pMem = tmp;
		for (int i = 0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];	
	}
	return *this;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	if (BitLen != bf.BitLen) 
		return false;

	for (int i = 0; i < MemLen; i++)
		if (pMem[i] != bf.pMem[i])
			return false;
	return true;
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
  return !(*this == bf);
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	TBitField result(std::max(BitLen, bf.BitLen));
	for (int i = 0; i < std::max(MemLen, bf.MemLen); i++) {
		TELEM lhs = (i < MemLen) ? pMem[i] : 0;
		TELEM rhs = (i < bf.MemLen) ? bf.pMem[i] : 0;
		result.pMem[i] = lhs | rhs;
	}
	return result;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	TBitField result(std::max(BitLen, bf.BitLen));
	for (int i = 0; i < std::max(MemLen, bf.MemLen); i++) {
		TELEM lhs = (i < MemLen) ? pMem[i] : 0;
		TELEM rhs = (i < bf.MemLen) ? bf.pMem[i] : 0;
		result.pMem[i] = lhs & rhs;
	}
	return result;
}

TBitField TBitField::operator~(void) // отрицание
{
	// Чтобы не инвертировать незначащие биты делаем побитовую операцию.
	TBitField result(BitLen);
	for (int i = 0; i < BitLen; i++) {
		if (GetBit(i) == 0) 
			result.SetBit(i);
	}
	return result;
}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
	std::string line;
	std::getline(istr, line);
	std::istringstream stream(line);
	std::vector<char> usefull_data;
	char ch;
	while(stream >> ch) {
		if (ch == '0' || ch == '1')
			usefull_data.push_back(ch);
	}
	TBitField newBitField(usefull_data.size());
	for (unsigned int i = 0; i < usefull_data.size(); i++) {
		if (usefull_data[i] == '1') 
			newBitField.SetBit(i);
	}
	bf = newBitField;
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	for (int i = 0; i < bf.BitLen; i++)
		if (bf.GetBit(i))
			ostr << 1;
		else
			ostr << 0;
	return ostr;
}
```
## Реализация класса TSet

```C++

// Множество - реализация через битовые поля

#include <vector>
#include <sstream>

#include "tset.h"

TSet::TSet(int mp) : BitField(mp), MaxPower(mp)
{
}

// конструктор копирования
TSet::TSet(const TSet &s) : BitField(s.BitField), MaxPower(s.MaxPower)
{
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : BitField(bf), MaxPower(bf.GetLength())
{
}

TSet::operator TBitField()
{
	return BitField;
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
	return BitField.GetBit(Elem);
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	BitField.ClrBit(Elem);
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание
{
	MaxPower = s.MaxPower;
	BitField = s.BitField;
	return *this;
}

int TSet::operator==(const TSet &s) const // сравнение
{
	return (MaxPower == s.MaxPower) && (BitField == s.BitField);
}

int TSet::operator!=(const TSet &s) const // сравнение
{
		return !(*this == s);
}

TSet TSet::operator+(const TSet &s) // объединение
{	
	return TSet(BitField | s.BitField);
}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	TSet result(*this);
	result.InsElem(Elem);
	return result;
}

TSet TSet::operator-(const int Elem) // разность с элементом
{
	TSet result(*this);
	result.DelElem(Elem);
	return result;
}

TSet TSet::operator*(const TSet &s) // пересечение
{
	return TSet(BitField & s.BitField);
}

TSet TSet::operator~(void) // дополнение
{
	return TSet(~BitField);
}

// перегрузка ввода/вывода

istream &operator>>(istream &istr, TSet &s) // ввод
{
	std::string line;
	std::getline(istr, line);
	std::istringstream stream(line);
	std::vector<int> usefull_data;
	int number;
	while(stream >> number) {
			usefull_data.push_back(number);
	}
	TSet  set(s.GetMaxPower());
	for (int i = 0; i < usefull_data.size(); i++) {
			set.InsElem(usefull_data[i]);
	}
	s = set;
	return istr;
}

ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{
	for (int i = 0; i < s.MaxPower; i++)
		if (s.IsMember(i))
			ostr << " " << i;
	return ostr;
}
```

## Демонстрация работы тестов
Для лучшего освоения Google Tests к уже имеющимся тестам было добавлено несколько следующих:

 * Проверка на двойную очистку бита.
 * Последовательное выполнение операций с тремя битовыми полями в строку.
 * Последовательное выполнение операций с тремя множествами в строку.

### Реализация новых тестов
```C++
#include "tbitfield.h"
#include "tset.h"

#include <gtest.h>
TEST(NewTests, can_double_clear_bit)
{
  TBitField bf(10);

  int bitIdx = 3;

  bf.SetBit(bitIdx);
  EXPECT_NE(0, bf.GetBit(bitIdx));
  bf.ClrBit(bitIdx);
  bf.ClrBit(bitIdx);
  EXPECT_EQ(0, bf.GetBit(bitIdx));
}

TEST(NewTests, or_operator_applied_to_3_bitfields_of_non_equal_size)
{
  TBitField bf1(1), bf2(3), bf3(4), expBf(4);
  // bf1 = 1
  bf1.SetBit(0);
  // bf2 = 011
  bf2.SetBit(1);
  bf2.SetBit(2);
  // bf3 = 0001
  bf3.SetBit(3);

  // expBf = 1111
  expBf.SetBit(0);
  expBf.SetBit(1);
  expBf.SetBit(2);
  expBf.SetBit(3);

  EXPECT_EQ(expBf, bf1 | bf2 | bf3);
}

TEST(NewTests, can_combine_3_sets_of_non_equal_size)
{
  const int size1 = 5, size2 = 7;
  const int size3 = 9;
  TSet set1(size1), set2(size2), set3(size3), expSet(size3);
  // set1 = {1, 2, 4}
  set1.InsElem(1);
  set1.InsElem(2);
  set1.InsElem(4);
  // set2 = {0, 1, 2, 6}
  set2.InsElem(0);
  set2.InsElem(1);
  set2.InsElem(2);
  set2.InsElem(6);

  // set3 = {3, 4, 8}
  set3.InsElem(3);
  set3.InsElem(4);
  set3.InsElem(8);

  // expSet = {0, 1, 2, 3, 4, 6, 8}
  expSet.InsElem(0);
  expSet.InsElem(1);
  expSet.InsElem(2);
  expSet.InsElem(3);
  expSet.InsElem(4);
  expSet.InsElem(6);
  expSet.InsElem(8);

  EXPECT_EQ(expSet, set1 + set2 + set3);
}
```
![работа новых тестов](./NewTests.png)

### Старые тесты выполняются успешно:

![старые тесты1](./TBitField.png)
![старые тесты2](./TSet.png)

### Пример решето Эратосфена выполняется успешно в обоих вариантах

![решето1](./sieve_default.png)
![решето2](./sieve_use_set.png)

## Вывод
Цели работы достигнуты. Реализованы методы классов "TSet" и "TBitField" и успешно пройдены все тесты основанные на фреймворке GoogleTests, 
а так же доказана работоспособность классов на основе имеющегося алгоритма поиска простых чисел.
Мне удалось освоить принцип работы с GoogleTests, улучшить навык работы с классами и ООП в целом.
