﻿##Отчет
###по самостоятельной работе №1 по дисциплине «Методы программирования 2»
##тема:
###«Множества на основе битовых полей»
#####Цель:

Цель данной работы — разработка структуры данных для хранения множеств с использованием битовых полей, а также освоение таких инструментов разработки программного обеспечения, как система контроля версий Git и фрэймворк для разработки автоматических тестов Google Test.

#####Задачи:

1.	Реализация класса битового поля TBitField согласно заданному интерфейсу.
2.	Реализация класса множества TSet согласно заданному интерфейсу.
3.	Обеспечение работоспособности тестов и примера использования.
4.	Реализация нескольких простых тестов на базе Google Test.
5.	Публикация исходных кодов в личном репозитории на BitBucket.

#####Перед началом работы были получены:

- Интерфейсы классов битового поля и множества (h-файлы)
- Готовый набор тестов для каждого из указанных классов
- Пример использования класса битового поля и множества для решения задачи поиска простых чисел с помощью алгоритма «Решето Эратосфена»

#####Начальные выводы после просмотра заголовочных файлов:

-	Мною было решено не добавлять никаких других методов ни в один из классов TSet и TBitField.
-	В классе TBitField следует быть осторожным с выходом индексов за пределы 0 и BitLen, в классе TSet также за пределы 0 и MaxPower.
-	В классе TSet будут часто использоваться функции из TBitField.
 
 
####Реализация класса битового поля TBitField согласно заданному интерфейсу.


#####Заголовочный файл tbitfield.h (интерфейс класса битового поля, полученный до начала выполнения работы)


```c++
#ifndef __BITFIELD_H__
#define __BITFIELD_H__

#include <iostream>

using namespace std;

typedef unsigned int TELEM;

class TBitField
{
private:
  int  BitLen; // длина битового поля - макс. к-во битов
  TELEM *pMem; // память для представления битового поля
  int  MemLen; // к-во эл-тов Мем для представления бит.поля

  // методы реализации
  int   GetMemIndex(const int n) const; // индекс в pМем для бита n
  TELEM GetMemMask (const int n) const; // битовая маска для бита n
public:
  TBitField(int len);
  TBitField(const TBitField &bf);
  ~TBitField();

  // доступ к битам
  int GetLength(void) const;      // получить длину (к-во битов)
  void SetBit(const int n);       // установить бит
  void ClrBit(const int n);       // очистить бит
  int  GetBit(const int n) const; // получить значение бита

  // битовые операции
  int operator==(const TBitField &bf) const; // сравнение
  int operator!=(const TBitField &bf) const; // сравнение
  TBitField& operator=(const TBitField &bf); // присваивание
  TBitField  operator|(const TBitField &bf); // операция "или"
  TBitField  operator&(const TBitField &bf); // операция "и"
  TBitField  operator~(void);                // отрицание

  friend istream &operator>>(istream &istr, TBitField &bf);       
  friend ostream &operator<<(ostream &ostr, const TBitField &bf); 
};
#endif
```


- tbitfield.h - Copyright (c) Гергель В.П. 07.05.2001


- Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)


- некоторые комментарии из tbitfield.h были удалены


#####Файл tbitfield.cpp (реализация класса битового поля)

```c++
#include "tbitfield.h"

TBitField::TBitField(int len):BitLen(len)// bitfield begining the account from 0 to len-1
{
	if (len > 0)
	{
		MemLen = (len + (sizeof(TELEM) * 8) * (len % (sizeof(TELEM) * 8) != 0) - len % (sizeof(TELEM) * 8)) / (sizeof(TELEM) * 8);
		pMem = new TELEM[MemLen];
		if (pMem != nullptr)
			for (int i = 0;i < MemLen;i++)
				pMem[i] = 0;//make all bits equal zero
		else
			throw 4;//if there is no memory throw error message
	}
	else
		throw 1;//if there is len <= 0 throw error message
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	MemLen = bf.MemLen;
	BitLen = bf.BitLen;
	pMem = new TELEM[MemLen];
	if (pMem != nullptr)
		for (int i = 0;i < MemLen;i++)
			pMem[i] = bf.pMem[i];
	else
		throw 4;//if there is no memory throw error message
}

TBitField::~TBitField()
{
	delete[]pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if ((n >= 0) && (n < BitLen))//check of borders
		return (n + (sizeof(TELEM) * 8) * (n % (sizeof(TELEM) * 8) != 0) - n % (sizeof(TELEM) * 8)) / (sizeof(TELEM) * 8) - 1 * (n != 0)*(n%(sizeof(TELEM)*8)!=0);
	else
		throw 2;//if there is a error of indexing throw error message
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	if ((n >= 0) && (n < BitLen))
		return (TELEM)(1 << (n % (sizeof(TELEM) * 8)));
	else
		throw 2;//if there is a error of indexing throw error message
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
	return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	if ((n >= 0) && (n < BitLen))
		pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] | GetMemMask(n);
	else
		throw 2;//if there is a error of indexing throw error message
}

void TBitField::ClrBit(const int n) // очистить бит
{
	if ((n >= 0) && (n < BitLen))
		pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] & (~GetMemMask(n));
	else
		throw 2;//if there is a error of indexing throw error message
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	if ((n >= 0) && (n < BitLen))
		return ((pMem[GetMemIndex(n)] & GetMemMask(n)) != 0);
	else
		throw 2;//if there is a error of indexing throw error message
}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	if (&bf != this)
	{
		delete[]pMem;
		MemLen = bf.MemLen;
		BitLen = bf.BitLen;
		pMem = new TELEM[MemLen];
		if (pMem != nullptr)
		{
			for (int i = 0;i < MemLen;i++)
				pMem[i] = bf.pMem[i];
			return (*this);
		}
		else
			throw 4;//if there is no memory throw error message
	}
	else
		throw 3;//if there is a conflict of addresses throw error message
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	int i = 0;//i==result
	if (BitLen == bf.BitLen)
	{
		while ((i < MemLen) && (bf.pMem[i] == pMem[i]))
			i++;
		if (i != MemLen)
			i = 0;
		else
			i = 1;
	}
	return i;
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
	return !(*this == bf);
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	TBitField temp((BitLen>bf.BitLen)?BitLen:bf.BitLen);//the largest BitLen
	for (int i = 0;i < temp.MemLen;i++)
		temp.pMem[i] = pMem[i] | bf.pMem[i];
	return temp;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	TBitField temp((BitLen>bf.BitLen) ? BitLen : bf.BitLen);//the largest BitLen
	for (int i = 0;i < temp.MemLen;i++)
		temp.pMem[i] = pMem[i] & bf.pMem[i];
	return temp;
}

TBitField TBitField::operator~(void) // отрицание
{
	TBitField temp(*this);
	if (MemLen != 1)
		for (int i = 0;i < MemLen - 1;i++)
			temp.pMem[i] = ~temp.pMem[i];
	for (int i = (MemLen-1)*sizeof(TELEM)*8;i < BitLen;i++)
		if (temp.GetBit(i) == 0)
			temp.SetBit(i);
		else
			temp.ClrBit(i);
	return temp;
}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
	int i =  0;
	char ch;
	while (i <bf.BitLen)
	{
		istr >> ch;
		if (ch == '0')
			bf.ClrBit(i++);
		else if (ch == '1')
			bf.SetBit(i++);
		else
			break;
	}
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	int i = 0;
	while (i <bf.BitLen)
		ostr << bf.GetBit(i++);
	return ostr;
}
```


- tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001


- Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. 
(19.04.2015)


#####Прохождение тестов реализованным классом TBitField


![](http://i.imgur.com/RdjX7Pp.png)


Время выполнения TBitField тестов: 35 ms.


Все тесты для класса TBitField успешно пройдены, что говорит о правильной реализации проверяемых методов.


######Запуск методов, проверка которых не предусмотрена изначально полученными тестами.


![](http://i.imgur.com/DfW69be.png)


#####Реализация класса битового поля TSet согласно заданному интерфейсу.


Заголовочный файл tset.h (интерфейс класса множества, полученный до начала выполнения работы)


-- tset.h включает в себя tbitfield.h


```c++
#ifndef __SET_H__
#define __SET_H__

#include "tbitfield.h"

class TSet
{
private:
  int MaxPower;       // максимальная мощность множества
  TBitField BitField; // битовое поле для хранения характеристического вектора
public:
  TSet(int mp);
  TSet(const TSet &s);       // конструктор копирования
  TSet(const TBitField &bf); // конструктор преобразования типа
  operator TBitField();      // преобразование типа к битовому полю

  // доступ к битам
  int GetMaxPower(void) const;     // максимальная мощность множества
  void InsElem(const int Elem);       // включить элемент в множество
  void DelElem(const int Elem);       // удалить элемент из множества
  int IsMember(const int Elem) const; // проверить наличие элемента в множестве

  // теоретико-множественные операции
  int operator== (const TSet &s) const; // сравнение
  int operator!= (const TSet &s) const; // сравнение
  TSet& operator=(const TSet &s);  // присваивание
  TSet operator+ (const int Elem); // объединение с элементом
                                   // элемент должен быть из того же универса
  TSet operator- (const int Elem); // разность с элементом
                                   // элемент должен быть из того же универса
  TSet operator+ (const TSet &s);  // объединение
  TSet operator* (const TSet &s);  // пересечение
  TSet operator~ (void);           // дополнение

  friend istream &operator>>(istream &istr, TSet &bf);
  friend ostream &operator<<(ostream &ostr, const TSet &bf);
};

#endif
```


- tset.h - Copyright (c) Гергель В.П. 07.05.2001


- Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)


- некоторые комментарии из tset.h были удалены


####Файл tset.cpp (реализация класса множества)

```c++
#include "tset.h"

TSet::TSet(int mp) : MaxPower(mp), BitField(mp)//using the constructor from tbitfield.h
{
}

// конструктор копирования
TSet::TSet(const TSet &s) : MaxPower(s.MaxPower),BitField(s.BitField)//using the constructor from tbitfield.h
{
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : BitField(bf),MaxPower(bf.GetLength())//using the constructor and method from tbitfield.h
{
}

TSet::operator TBitField()
{
	TBitField temp(BitField);
	return temp;
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
    return (BitField.GetBit(Elem)!=0);//using the method from tbitfield.h
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	BitField.SetBit(Elem);//using the method from tbitfield.h
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	BitField.ClrBit(Elem);//using the method from bitfield.h
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание
{
	BitField = s.BitField;
	MaxPower = s.MaxPower;
	return *this;
}

int TSet::operator==(const TSet &s) const // сравнение
{
	if (MaxPower == s.MaxPower)
		if (BitField == s.BitField)//using the method from tbitfield.h
			return 1;
		else
			return 0;
	else
		return 0;
}

int TSet::operator!=(const TSet &s) const // сравнение
{
	return !(*this == s);
}

TSet TSet::operator+(const TSet &s) // объединение
{
	TSet temp(*this);
	temp.BitField = temp.BitField | s.BitField;//using the method from tbitfield.h
	return temp;
}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	TSet temp(*this);
	temp.InsElem(Elem);
	return temp;
}

TSet TSet::operator-(const int Elem) // разность с элементом
{
	TSet temp(*this);
	temp.DelElem(Elem);
	return temp;
}

TSet TSet::operator*(const TSet &s) // пересечение
{
	TSet temp(*this);
	temp.BitField = temp.BitField & s.BitField;//using the method from tbitfield.h
	return temp;
}

TSet TSet::operator~(void) // дополнение
{
	TSet temp(*this);
	temp.BitField = ~temp.BitField;//using the method from tbitfield.h
	return temp;
}

// перегрузка ввода/вывода

istream &operator>>(istream &istr, TSet &s) // ввод
{
	int temp;
	char ch;
	do 
	{
		istr >> ch;
	}
	while (ch != '{');
	do
	{
		istr >> temp;
		s.InsElem(temp);
		do 
		{ 
			istr >> ch; 
		} 
		while ((ch != ',') && (ch != '}'));
	}
	while (ch != '}');
	return istr;
}

ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{
	int i = 0;
	bool check = 0;
	ostr << "{";
	while (i < s.MaxPower)
	{
		if (s.IsMember(i) != 0)
		{
			if(check!=0)// to not display the excess ','
			{
				ostr << ",";
			}
			ostr << i;
			check = 1;
		}
		i++;
	}
	ostr << "}";
	return ostr;
}
```


- tset.cpp - Copyright (c) Гергель В.П. 07.05.2001


- Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)



#####Прохождение тестов реализованным классом TSet



![](http://i.imgur.com/GvqCfZc.png)



Время выполнения TSet тестов: 45 ms.



Все тесты для класса TSet успешно пройдены, что говорит о правильной реализации проверяемых методов.



#####Запуск методов, проверка которых не предусмотрена изначально полученными тестами.



![](http://i.imgur.com/2boOgBJ.png)



Так как большинство методов класса TSet реализовано с помощью методов класса TBitField, и отличаются классы лишь за счет одного поля, показывать запуск некоторых методов TSet не имеет смысла (например, логическое равенство или неравенство).



#####Результат прохождения всех изначально полученных тестов:



 ![](http://i.imgur.com/2CEjvVt.png)
 

Время выполнения: 116 ms.

####Решето Эратосфена



![](http://i.imgur.com/kFkJTwK.png)



#####Таблица простых чисел до 1000

![](http://i.imgur.com/FhtsaiF.png)

Как видно из таблицы, простых чисел до 150 ровно 35 и они совпадают с теми, что были получены в результате работы Решета Эратосфена.
Сэмпл-приложение (Решето Эратосфена) успешно справилось с поставленной задачей.

##Выводы: 

1) при разработке некоторых классов полезно использовать уже готовые реализации других классов со схожими методами и логикой; 

2) система контроля версий «Git» позволяет перемещаться по коммитам и возвращает сохраненные наработки, даже если файлы были испорчены или удалены; 

3) система тестирования «Google Test» дает возможность легко и быстро найти ошибки в реализации методов.
