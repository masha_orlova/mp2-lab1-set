// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#include "tbitfield.h"
#include <math.h>

TBitField::TBitField(int len)
{
		if (len < 0)
			throw "Lenght of field must be natural number";
		BitLen = len;
		MemLen = (len / (sizeof TELEM * 8) + (bool)(len % (sizeof TELEM * 8)));
		pMem = new TELEM[MemLen];
		if (pMem == nullptr)
			throw "not enough memory";
		for (int i = 0; i < MemLen; i++)
			pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
		BitLen = bf.GetLength();
		MemLen = (BitLen / (sizeof TELEM * 8) + (bool)(BitLen % (sizeof TELEM * 8)));
		pMem = new TELEM[MemLen];
		if (pMem == nullptr)
			throw "not enough memory";
		for (int i = 0; i < MemLen; i++)
			pMem[i] = 0;
		for (int i = 0; i < bf.GetLength(); i++) 
			if (bf.GetBit(i))
				SetBit(i);
			else
				ClrBit(i);
}

TBitField::~TBitField()
{
		delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
		if (n < 0 || n >= BitLen)
			throw "wrong n";
		return  n / (sizeof TELEM * 8);
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
		if (n >= BitLen || n < 0)
			throw "wrong n";
		return 1 << (n - GetMemIndex(n) * sizeof TELEM * 8);
}

// доступ к битам битового поля

int TBitField::GetLength() const // получить длину (к-во битов)
{
	 return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
		if (n >= BitLen || n < 0)
			throw "wrong n";
		pMem[GetMemIndex(n)] |= GetMemMask(n);
}

void TBitField::ClrBit(const int n) // очистить бит
{
		if (n >= BitLen || n < 0)
			throw "wrong n";
		pMem[GetMemIndex(n)] &= ~GetMemMask(n);
}

int TBitField::GetBit(const int n) const // получить значение бита
{
		if (n >= BitLen || n < 0)
			throw "wrong n";
		if ((pMem[GetMemIndex(n)] & GetMemMask(n)) == GetMemMask(n))
			return 1;
		else
			return 0;
}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	if (this != &bf)
	{
		BitLen = bf.GetLength();
		MemLen = (BitLen / (sizeof TELEM * 8) + (bool)(BitLen % (sizeof TELEM * 8)));
		pMem = new TELEM[MemLen];
		for (int i = 0; i < MemLen; i++)
			pMem[i] = 0;
		for (int i = 0; i < GetLength(); i++)
			if (bf.GetBit(i))
				SetBit(i);
			else
				ClrBit(i);
	}
		return *this;
}	

int TBitField::operator==(const TBitField &bf) const // сравнение
{
		int result;
		result = 1;
		if (bf.GetLength() == BitLen)
		{
			for (int i = 0; (i < BitLen && result == 1); i++)
				if (GetBit(i) != bf.GetBit(i))
					result = 0;
		}
		else
			result = 0;
		return result;
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
		return !(*this == bf);
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
		TBitField result((BitLen >= bf.GetLength()) ? *this : bf);
		for (int i = 0; i < ((BitLen >= bf.GetLength()) ? bf.GetLength() : BitLen); i++)
			if (GetBit(i) | bf.GetBit(i))
				result.SetBit(i);
			else
				result.ClrBit(i);
		return result;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
		TBitField result((BitLen >= bf.GetLength()) ? BitLen : bf.GetLength());
		for (int i = 0; i < ((BitLen >= bf.GetLength()) ? bf.GetLength() : BitLen); i++)
			if (GetBit(i) & bf.GetBit(i))
				result.SetBit(i);
			else
				result.ClrBit(i);
		return result;
}

TBitField TBitField::operator~() // отрицание
{
		TBitField temp(*this);
		for (int i = 0; i < BitLen; i++)
			temp.GetBit(i) ? temp.ClrBit(i) : temp.SetBit(i);
		return temp;
}

// ввод/вывод

istream &operator >> (istream &istr, TBitField &bf) // ввод
{
		char value;
		for (int i = 0; i < bf.BitLen; i++)
		{
			istr >> value;
			if (value == 0)
				bf.ClrBit(i);
			else if (value == 1)
				bf.SetBit(i);
			else
				break;
		}
		return istr;

}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
		for (int i = 0; i < bf.BitLen; i++) 
		{
			ostr << bf.GetBit(i);
		}
		return ostr;

}