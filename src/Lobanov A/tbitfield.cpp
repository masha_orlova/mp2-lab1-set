// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле
#include <cmath>
#include "tbitfield.h"
const int BITS = sizeof(TELEM) * 8;
const unsigned int SHIFT = log(BITS) / log(2);

TBitField::TBitField(int len)
{
    BitLen = len;
	if (len < 0)
	  throw "ERROR 1"; //размер поля не может быть отрицательным числом
    MemLen = ( len + BITS ) >> SHIFT; 
    pMem = new TELEM[MemLen];
    if (pMem != NULL)
       for (int i=0; i < MemLen; i++)
			pMem[i] = 0;
    else  
	   throw "ERROR 2"; // память не выделилась

}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	if (pMem != NULL) 
		for (int i=0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];	
	else 
		throw "ERROR 2"; 
}

TBitField::~TBitField()
{
   delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
   if ( n >= 0 && n < BitLen )
      return n >> SHIFT;
   else
      throw "ERROR 3"; //выход за пределы битового поля 
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
   if ( n >= 0 && n < BitLen )
      return 1 << (n % (BITS - 1));
   else
      throw "ERROR 3";
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
  return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
   if (( n >= 0 ) && ( n < BitLen)) 
      pMem[GetMemIndex(n)] |= GetMemMask(n);
   else
	  throw "ERROR 3";
} 

void TBitField::ClrBit(const int n) // очистить бит
{
	if (( n > -1 ) && ( n < BitLen)) 
      pMem[GetMemIndex(n)] &=~GetMemMask(n);
   else
	  throw "ERROR 3";
}

int TBitField::GetBit(const int n) const // получить значение бита
{
   if (( n >= 0 ) && ( n < BitLen)) 
      return pMem[GetMemIndex(n)] & GetMemMask(n);
   else
	  throw "ERROR 3";  
}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
   if ( this != &bf ) {
	   BitLen = bf.BitLen;
	   MemLen = bf.MemLen;
	   if (pMem == NULL)
		   throw "ERROR 4"; // объетка не существует
	   delete[] pMem;
	   pMem = new TELEM[MemLen];
	   for ( int i=0; i < MemLen; i++)
		   pMem[i] = bf.pMem[i];
       }
   return *this;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
   if ( BitLen != bf.BitLen )
      return 0;
   for ( int i=0; i < MemLen; i++)
	  if ( pMem[i] != bf.pMem[i] )
         return 0;
   return 1;
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
   return !(*this == bf);
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
   if ( bf.BitLen > BitLen )
	   BitLen = bf.BitLen;
   TBitField tmp(BitLen);
   for ( int i=0; i < MemLen; i++ )
	   tmp.pMem[i] = pMem[i];
   for ( int i=0; i < bf.MemLen; i++ )
	   tmp.pMem[i] |= bf.pMem[i];
   return tmp;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
   if ( bf.BitLen > BitLen )
	   BitLen = bf.BitLen;
   TBitField tmp(BitLen);
   for ( int i=0; i < MemLen; i++ )
	   tmp.pMem[i] = pMem[i];
   for ( int i=0; i < bf.MemLen; i++ )
	   tmp.pMem[i] &= bf.pMem[i];
   return tmp;
}

TBitField TBitField::operator~(void) // отрицание
{
   TBitField tmp(BitLen);
   for ( int i=0; i < BitLen; i++ ){
	   if ( GetBit(i) == 0 )
		   tmp.SetBit(i);
	   else  
		   tmp.ClrBit(i);  
   }
   return tmp;
}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
   int i = 0;
   char st;
   istr >> st;
   while ( i < bf.BitLen ) {
      if ( st == '0' )
		  bf.ClrBit(i++);
	  if ( st == '1' )
		  bf.SetBit(i++);
	  else 
		  break;
   }
   return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
   for ( int i=0; i < bf.BitLen; i++ )
	   ostr << bf.GetBit(i);
   return ostr;
}
