## 1. Введение

### 1.1 Цель работы

Цель данной работы — разработка структуры данных для хранения множеств с использованием битовых полей, а также освоение таких инструментов разработки программного обеспечения, как система контроля версий Git и фрэймворк для разработки автоматических тестов Google Test.

### 1.2 Краткое описание работы

В языке программирования С++ уже имеется такой тип данных, как структура с битовыми полями, однако работа с ним крайне неудобна. Ведь не получится использовать стандартные побитовые операции между двумя объектами такого типа, а также трудно реализовать удобный доступ к элементам битового поля. Поэтому идеальным решением будет использование возможностей ООП. В качестве элемента, хранящего в себе биты, будет использован *unsigned int*, так как он наиболее простым способом представляется в памяти, а поразрядные операции языка С++ помогут нам реализовать основные методы будущего класса.

### 1.3 Исключения, возникающие в ходе программы

Ниже перечислены исключения, которые могут быть вызваны при выполнении программы с использованием классов **TBitField** и **TSet**.

- 1 - Попытка создания объекта с отрицательным размером.
- 2 - Не выделилась память для объекта.
- 3 - Указан некорректный индекс при работе с отдельным битом (элементом множества)

## 2. Код программы и тесты

### 2.1 Код реализации класса TBitField

    #include "tbitfield.h"
    #include <string>
    
    TBitField::TBitField(int len)
    {
    	BitLen = len;
    
    	if (len < 0)
    		throw 1;
    	if (len % (sizeof(TELEM) * 8) == 0)
    		MemLen = len / (sizeof(TELEM) * 8);
    	else
    		MemLen = len / (sizeof(TELEM) * 8) + 1;
    
    	pMem = new TELEM[MemLen];
    
    	if (pMem == nullptr)
    		throw 2;
    
    	for (int i = 0; i < MemLen; i++)
    		pMem[i] = 0;
    }
    
    TBitField::TBitField(const TBitField &bf) // конструктор копирования
    {
    	BitLen = bf.BitLen;
    	MemLen = bf.MemLen;
    
    	pMem = new TELEM[MemLen];
    
    	if (pMem == nullptr)
    		throw 2;
    
    	for (int i = 0; i < MemLen; i++)
    		pMem[i] = bf.pMem[i];
    }
    
    TBitField::~TBitField()
    {
    	delete[] pMem;
    }
    
    int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
    {
    	if ((n + 1) % (sizeof(TELEM) * 8) == 0)
    		return (n + 1) / (sizeof(TELEM) * 8) - 1;
    	else
    		return (n + 1) / (sizeof(TELEM) * 8);
    }
    
    TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
    {
    	TELEM temp = 1;
    	int shift = n % (sizeof(TELEM) * 8);
    
    	temp = temp << shift;
    
    	return temp;
    }
    
    // доступ к битам битового поля
    
    int TBitField::GetLength(void) const // получить длину (к-во битов)
    {
    	return BitLen;
    }
    
    void TBitField::SetBit(const int n) // установить бит
    {
    	if (n >= 0 && n < BitLen)
    		pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] | GetMemMask(n);
    	else
    		throw 3;
    }	
    
    void TBitField::ClrBit(const int n) // очистить бит
    {
    	if (n >= 0 && n < BitLen)
    		pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] & ~GetMemMask(n);
    	else
    		throw 3;
    }
    
    int TBitField::GetBit(const int n) const // получить значение бита
    {
    	if (n >= 0 && n < BitLen)
    		return pMem[GetMemIndex(n)] & GetMemMask(n) ? 1 : 0;
    	else
    		throw 3;
    }
    
    // битовые операции
    
    TBitField& TBitField::operator=(const TBitField &bf) // присваивание
    {
    	if (this == &bf)
    		return *this;
    	else
    	{
    		BitLen = bf.BitLen;
    		MemLen = bf.MemLen;
    
    		delete[] pMem;
    
    		pMem = new TELEM[MemLen];
    
    		if (pMem == nullptr)
    			throw 2;
    
    		for (int i = 0; i < MemLen; i++)
    			pMem[i] = bf.pMem[i];
    
    		return *this;
    	}	
    }
    
    int TBitField::operator==(const TBitField &bf) const // сравнение
    {
    	if (BitLen != bf.BitLen)
    		return 0;
    	else
    	{
    		for (int i = 0; i < BitLen; i++)
    			if (GetBit(i) != bf.GetBit(i))
    				return 0;
    	}
    
    	return 1;
    }
    
    int TBitField::operator!=(const TBitField &bf) const // сравнение
    {
    	return !(*this == bf);
    }
    
    TBitField TBitField::operator|(const TBitField &bf) // операция "или"
    {
    	if (BitLen < bf.BitLen)
    	{
    		TBitField tmp(bf);
    		for (int i = 0; i < MemLen; i++)
    			tmp.pMem[i] = tmp.pMem[i] | pMem[i];
    		return tmp;
    	}
    	else
    	{
    		TBitField tmp(*this);
    		for (int i = 0; i < bf.MemLen; i++)
    			tmp.pMem[i] = tmp.pMem[i] | bf.pMem[i];
    		return tmp;
    	}
    }
    
    TBitField TBitField::operator&(const TBitField &bf) // операция "и"
    {
    	if (BitLen < bf.BitLen)
    	{
    		TBitField tmp(bf);
    		for (int i = 0; i < MemLen; i++)
    			tmp.pMem[i] = tmp.pMem[i] & pMem[i];
    		return tmp;
    	}
    	else
    	{
    		TBitField tmp(*this);
    		for (int i = 0; i < bf.MemLen; i++)
    			tmp.pMem[i] = tmp.pMem[i] & bf.pMem[i];
    		return tmp;
    	}
    }
    
    TBitField TBitField::operator~(void) // отрицание
    {
    	TBitField tmp(*this);
    
    	for (int i = 0; i < MemLen; i++)
    		tmp.pMem[i] = ~tmp.pMem[i];
    
    	return tmp;
    }
    
    // ввод/вывод
    
    istream &operator>>(istream &istr, TBitField &bf) // ввод
    {
    	string s;
    	istr >> s;
    
    	for (int i = 0; i < s.size(); i++)
    	{
    		if (s[i] == '0')
    			bf.ClrBit(i);
    		else
    			bf.SetBit(i);
    	}
    
    	return istr;
    }
    
    ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
    {
    	for (int i = 0; i < bf.BitLen; i++)
    		ostr << bf.GetBit(i);
    
    	return ostr;
    }

### 2.2 Код реализации класса TSet

    #include "tset.h"
    
    TSet::TSet(int mp) : BitField(mp)
    {
    	MaxPower = mp;
    }
    
    // конструктор копирования
    TSet::TSet(const TSet &s) : BitField(s.BitField)
    {
    	MaxPower = s.MaxPower;
    }
    
    // конструктор преобразования типа
    TSet::TSet(const TBitField &bf) : BitField(bf)
    {
    	MaxPower = bf.GetLength();
    }
    
    TSet::operator TBitField()
    {
    	TBitField tmp(BitField);
    	return tmp;
    }
    
    int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
    {
    	return MaxPower;
    }
    
    int TSet::IsMember(const int Elem) const // элемент множества?
    {
    	return BitField.GetBit(Elem) ? 1 : 0;
    }
    
    void TSet::InsElem(const int Elem) // включение элемента множества
    {
    	BitField.SetBit(Elem);
    }
    
    void TSet::DelElem(const int Elem) // исключение элемента множества
    {
    	BitField.ClrBit(Elem);
    }
    
    // теоретико-множественные операции
    
    TSet& TSet::operator=(const TSet &s) // присваивание
    {
    	MaxPower = s.MaxPower;
    	BitField = s.BitField;
    	return *this;
    }
    
    int TSet::operator==(const TSet &s) const // сравнение
    {
    	return BitField == s.BitField;
    }
    
    int TSet::operator!=(const TSet &s) const // сравнение
    {
    	return BitField != s.BitField;
    }
    
    TSet TSet::operator+(const TSet &s) // объединение
    {
    	TSet tmp(BitField | s.BitField);
    	return tmp;
    }
    
    TSet TSet::operator+(const int Elem) // объединение с элементом
    {
    	TSet tmp(*this);
    
    	tmp.InsElem(Elem);
    
    	return tmp;
    }
    
    TSet TSet::operator-(const int Elem) // разность с элементом
    {
    	TSet tmp(MaxPower);
    
    	tmp.InsElem(Elem);
    	
    	tmp = tmp * *this;
    
    	return tmp;
    }
    
    TSet TSet::operator*(const TSet &s) // пересечение
    {
    	TSet tmp(*this);
    
    	tmp.BitField = tmp.BitField & s.BitField;
    	tmp.MaxPower = tmp.BitField.GetLength();
    
    	return tmp;
    }
    
    TSet TSet::operator~(void) // дополнение
    {
    	TSet tmp(*this);
    
    	tmp.BitField = ~tmp.BitField;
    
    	return tmp;
    }
    
    // перегрузка ввода/вывода
    
    istream &operator>>(istream &istr, TSet &s) // ввод
    {
    	int i;

		while (true)
		{
			cin >> i;
	
			if (i == -1)
				break;
			else
				s.InsElem(i);
		}

		return istr;
    }
    
    ostream& operator<<(ostream &ostr, const TSet &s) // вывод
    {
    	cout << "{ ";
    
    	for (int i = 0; i < s.BitField.GetLength() - 1; i++)
    		if (s.IsMember(i))
    			cout << i << " ";
    
    	cout << "}";
    
    	return ostr;
    }

### 2.3 Демонстрация работы тестов

Для лучшего освоения Google Tests к уже имеющимся тестам было добавлено несколько следующих:

- Проверка на двойную очистку бита
- Последовательное выполнение операций с тремя битовыми полями в строку
- Последовательное выполнение операций с тремя множествами в строку

Ниже приведен код добавленных тестов:

    TEST(TBitField, double_cleaning)
    {
    	const int size = 1;
    	TBitField bf1(size);
    
    	bf1.SetBit(0);
    
    	bf1.ClrBit(0);
    	bf1.ClrBit(0);
    
    	EXPECT_EQ(0, bf1.GetBit(0));
    }

    TEST(TBitField, or_operator_with_three_bitfields)
    {
    	const int size = 4;
    	TBitField bf1(size), bf2(size), bf3(size), bf4(size), bf5(size);
    
    	bf1.SetBit(0);
    	bf2.SetBit(2);
    	bf3.SetBit(3);
    
    	bf5.SetBit(0);
    	bf5.SetBit(2);
    	bf5.SetBit(3);
    
    	//Ожидается 1011
    	bf4 = bf1 | bf2 | bf3;
    	EXPECT_EQ(bf4, bf5);
    }
    
    TEST(TSet, or_operator_with_three_sets)
    {
    	const int size = 4;
    	TSet set1(size), set2(size), set3(size), set4(size), set5(size);
    
    	set1.InsElem(0);
    	set2.InsElem(2);
    	set3.InsElem(3);
    
    	set5.InsElem(0);
    	set5.InsElem(2);
    	set5.InsElem(3);
    
    	//Ожидается 1011
    	set4 = set1 + set2 + set3;
    	EXPECT_EQ(set4, set5);
    }

Результат работы Google Tests:

![Google-тесты](https://pp.vk.me/c837732/v837732338/2681/QmlI6NgVkO4.jpg)

Результат работы программы "Решето Эратосфена" на битовом поле с верхней границей - 100

![Решето Эратосфена](https://pp.vk.me/c837732/v837732338/2666/xvdGTDv_fxQ.jpg)

Результат работы программы "Решето Эратосфена" на множестве с верхней границей - 100

![Решето Эратосфена](https://pp.vk.me/c837732/v837732338/266f/mCHkxgz_GY8.jpg)

## 3.Заключение

С помощью возможностей ООП в С++ был реализован такой часто используемый в математике и во многих других областях науки объект, как множество. Были применены такие возможности ООП, как перегрузка операторов, дружественные функции и многое другое, что сделало наше множество удобным в использовании. Для тестирования была использована система тестов Google Tests, о которой еще много предстоит узнать, но некоторые шаги уже сделаны: написано несколько своих тестов. Также помощь в проверке работоспособности класса оказала программа, реализующая поиск простых чисел по методу, называемым "Решето Эратосфена".

После выполнения работы стал очевидным тот факт, что тестов необходимо делать много, важно тестировать каждую функцию, проверять все, что может потенциально вызвать ошибку. И даже несмотря на то, что "Решето Эратосфена" работает корректно, весьма вероятно, что где-то в глубине нашего функционала кроется великое множество ошибок.

