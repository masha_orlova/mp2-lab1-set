using namespace std;
#include <iostream>
#include "tbitfield.h"

int main()
{
	TBitField bf(100);

  int sum = 0;
  for (int i = 0; i < bf.GetLength(); i++)
  {
    sum += bf.GetBit(i);
  }
  cout << sum;
	return 0;
}