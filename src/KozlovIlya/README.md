# Цель работы
Разработка структуры данных для хранения множеств с
использованием битовых полей, а также освоение таких инструментов разработки
программного обеспечения, как система контроля версий git и фрэймворк для
разработки автоматических тестов Google Test.
 ###    Предполагается:
 1. Реализация класса битового поля __`TBitField`__ согласно заданному интерфейсу.
2. Реализация класса множества __`TSet`__ согласно заданному интерфейсу.
3. Обеспечение работоспособности тестов и примера использования.
4. Реализация нескольких простых тестов на базе Google Test.
5. Публикация исходных кодов в личном репозитории на BitBucket
---
# Реализация битового поля
### Описание класса TBitField:
Битовое поле представленно в виде массива __`pMem`__ с типом __`TELEM (unsigned int)`__,
__`BitLen`__ - количество битов в нашем поле,на основе __`BitLen`__ вычисляется __`MemLen`__ - количество
элементов __`pMem`__.
В классе реализованны методы доступа к отдельным битам, операции над битовыми полями и
операторы ввода/выводы.
Нумерация битов идет справа налево,первый бит имеет индекс __`(0)`__,а последний __`(Bitlen-1)`__.
### Исключения - обрабтка:
1. Недопустимая длинна битового поля - длинна битового поля меняется на **1**.
2. Бита к которому мы обращаемся не существует - обращение идет к первому биту поля.
3. Элемента к которому мы обращаемся не существует - обращение идет к первому элементу множества
---
# Программный код
### Реализация битового поля __`TBField.cpp`__:
```c++

#include "tbitfield.h"
#include <cmath> // Подключаем для возможности использовать log();

const unsigned short BDigit = sizeof(TELEM)*8;
const unsigned short Shift = ceil(log((double)BDigit)/log(2.0));


TBitField::TBitField(int len)
{
	if (len < 1)
	{
		throw "\n\nError 1! Inaccessible length of the bitfield!\n\n";
	}

	BitLen = len;
	MemLen = (BitLen + (BDigit - 1)) >> Shift;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
	pMem[i] = 0;

}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
	pMem[i] = bf.pMem[i];
}

TBitField::~TBitField()
{
	delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if (n >= BitLen || n < 0)
	{
		throw "\n\nError 2! This bit does not exist!\n\n";
	}

	return n >> Shift;
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	if (n >= BitLen || n < 0)
	{
		throw "\n\nError 2! This bit does not exist!\n\n";
	}

	return (1 << (n % (sizeof(TELEM) * 8)));
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
  return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	if (n >= BitLen || n < 0)
	{
		throw "\n\nError 2! This bit does not exist!\n\n";
	}

	pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] | GetMemMask(n);
}

void TBitField::ClrBit(const int n) // очистить бит
{
	if (n >= BitLen || n < 0)
	{
		throw "\n\nError 2! This bit does not exist!\n\n";
	}

	pMem[GetMemIndex(n)] &= (~GetMemMask(n));
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	if (n >= BitLen || n < 0)
	{
		throw "\n\nError 2! This bit does not exist!\n\n";
	}

	return (bool)(pMem[GetMemIndex(n)] & GetMemMask(n));

}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	delete[] pMem;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
		pMem[i] = bf.pMem[i];

	return *this;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	if (BitLen == bf.BitLen)
	{
		for (int i = 0; i < BitLen; i++)
		if (GetBit(i) != bf.GetBit(i))
			return 0;
		return 1;
	}
	else
		return 0;
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
	if (BitLen == bf.BitLen)
	{
		for (int i = 0; i < BitLen; i++)
		if (GetBit(i) != bf.GetBit(i))
			return 1;
		return 0;
	}
	else
		return 1;
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	int len = BitLen;

	if (bf.BitLen > BitLen)
		len = bf.BitLen;

	TBitField data(len);

	for (int i = 0; i < MemLen; i++)
		data.pMem[i] = pMem[i];

	for (int i = 0; i < bf.MemLen; i++)
		data.pMem[i] |= bf.pMem[i];

	return data;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	{
	int len = BitLen;

	if (bf.BitLen > BitLen)
		len = bf.BitLen;

	TBitField data(len);

	for (int i = 0; i < MemLen; i++)
		data.pMem[i] = pMem[i];

	for (int i = 0; i < bf.MemLen; i++)
		data.pMem[i] &= bf.pMem[i];

	return data;
}
}

TBitField TBitField::operator~(void) // отрицание
{
	TBitField temp(BitLen);
	for (int i = 0; i < MemLen; i++)
	temp.pMem[i] = ~pMem[i];
	return temp;	

}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
	char data;
	
	for (int i = 0; i<bf.BitLen; i++)
	{
		tryagain:
		istr >> data;
		if ( (data == '1') || (data = '0') )

			if (data == '1')
				bf.SetBit(i);
			else
				bf.ClrBit(i);

		else
			cout<<"\b";
			goto tryagain;
	}
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
for (int i = 0; i < bf.BitLen; i++)
if (bf.GetBit(i))
ostr << '1';
else
ostr << '0';
return ostr;
}
```

### Реализация __`TSet.cpp`__:
#
```c++

#include "tset.h"

TSet::TSet(int mp) : BitField(mp)
{
	MaxPower = mp;
}

// конструктор копирования
TSet::TSet(const TSet &s) : BitField(s.BitField)
{
	MaxPower = s.MaxPower;
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : BitField(bf)
{
	MaxPower = bf.GetLength();
}

TSet::operator TBitField()
{
	return BitField;
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
		if (Elem < 0)
	{
		throw "\n\nError 3! This element does not exist!\n\n";
	}


	return BitField.GetBit(Elem);
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	if (Elem < 0)
	{
		throw "\n\nError 3! This element does not exist!\n\n";
	}

	BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	if (Elem < 0)
	{
		throw "\n\nError 3! This element does not exist!\n\n";
	}

	BitField.ClrBit(Elem);
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание
{
	MaxPower = s.MaxPower;
	BitField = s.BitField;
	return *this;
}

int TSet::operator==(const TSet &s) const // сравнение
{
	if(BitField == s.BitField)
		return 1;
	return 0;
}

int TSet::operator!=(const TSet &s) const // сравнение
{
	if(BitField == s.BitField)
		return 0;
	return 1;
}

TSet TSet::operator+(const TSet &s) // объединение
{
	TSet DATA(BitField | s.BitField);
		return DATA;

}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	TSet DATA = *this;
	DATA.InsElem(Elem);
	return DATA;
}

TSet TSet::operator-(const int Elem) // разность с элементом
{
	TSet DATA = *this;
	DATA.DelElem(Elem);
	return DATA;

}

TSet TSet::operator*(const TSet &s) // пересечение
{
	TSet DATA(BitField & s.BitField);
	return DATA;
}

TSet TSet::operator~(void) // дополнение
{
	TSet DATA = ~BitField;
	return DATA;
}

// перегрузка ввода/вывода

istream &operator>>(istream &istr, TSet &s) // ввод
{
	int el;
	char data;

	cout<<"\n\n{";

	for (int i=0; i<s.MaxPower; i++)
	{
		tryagain:
		istr>>data;

		if (isdigit(data))
		{
			el=data;
			s.InsElem(el);
			cout<<" , ";
		}

		else
			cout<<'\t';
			goto tryagain;
	}

	cout<<"}\n\n";
	return istr;
}

ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{
	bool first = true;
	ostr <<"{";
	for(int i = 0; i < s.MaxPower; i++)
		{
			if(s.IsMember(i))
			{
				if(!first)
					ostr <<" , ";
				else
					first = false;
				ostr << i;
			}
		}
	ostr <<"}";
	return ostr;
}
```
---
# Тестирование
#
> К приложенным тестам напишем несколько своих:
* Проврка корректности битовой операции в одну строку с несколькими полями
* Проверка двойной очистки бита
* Проверка корректности сложения в одну строку нескольких множеств
### Программный код тестов:
```c++
TEST(TBitField, or_bitfield) 
{
int size = 5;
TBitField bf1(size),bf2(size),bf3(size),res1(size),res2(size);
bf1.SetBit(0);
bf2.SetBit(0);
bf3.SetBit(3);
bf3.SetBit(4);
res1 = bf1 | bf2 | bf3;
res2.SetBit(0);
res2.SetBit(3);
res2.SetBit(4);
EXPECT_EQ(res1, res2);
}


TEST(TBitField, 2nd_time_cleanig)
{
TBitField bf(5);
bf.SetBit(3);
bf.ClrBit(3);
EXPECT_EQ(0, bf.GetBit(3));
bf.ClrBit(3);
EXPECT_EQ(0, bf.GetBit(3));
}


TEST(TSet, plus_tset)
{
int size = 5;
TSet set1(size),set2(size),set3(size),res1(size),res2(size);
set1.InsElem(0);
set2.InsElem(0);
set3.InsElem(3);
set3.InsElem(4);
res1 = set1 + set2 + set3;
res2.InsElem(0);
res2.InsElem(3);
res2.InsElem(4);
EXPECT_EQ(res1, res2);
}
```
### Результаты выполнения программы:
* Все тесты (включая добавленные нами) пройдены.
* Решето Эратосфена работает с как с __`TBitField`__, так и с __`TSet`__.
* Затраченное время на построение Решето Эратосфена: __`00:00:05.43`__.
#
![Sample](https://pp.vk.me/c604418/v604418181/3878e/QAI5bKEoxQk.jpg)
![Tests](https://pp.vk.me/c604418/v604418181/3f435/OqfUOLqCvf0.jpg)
--- 
# Выводы
###### В результате выполнения работы мы научились:
+ Пользоваться полезным фрэймворком __*Google Tests*__
+ Работать с чужим кодом
+ Методике агрегации