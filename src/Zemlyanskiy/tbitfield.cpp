// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#include "tbitfield.h"
#define tel (TELEM) (sizeof(TELEM)* 8)

TBitField::TBitField(int len) :BitLen(len)
{
	if (len > 0)
		 {
			MemLen = (len + (tel) * (len % (tel) != 0) - len % (tel)) / (tel);
			pMem = new TELEM[MemLen];
			if (pMem != nullptr)
				for (int i = 0; i < MemLen; i++)
					pMem[i] = 0;//освободим память
		 }
	else
		throw 1;//на случай len <= 0
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	MemLen = bf.MemLen;
	BitLen = bf.BitLen;
	pMem = new TELEM[MemLen];
		 if (pMem != nullptr)
			for (int i = 0; i < MemLen; i++)
				pMem[i] = bf.pMem[i];
	
}

TBitField::~TBitField()
{
	delete pMem;
	pMem = nullptr;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if ((n >= 0) && (n < BitLen))//определим границы
		 return (n + (tel) * (n % (tel) != 0) - n % (tel)) / (tel) - 1 * (n != 0)*(n % (tel) != 0);
	else
		 throw 2;
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	if ((n >= 0) && (n < BitLen))
		 return (TELEM)(1 << (n % (tel)));
	else
		 throw 2;
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
  return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	if ((n >= 0) && (n < BitLen))
		 pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] | GetMemMask(n);
	else
		 throw 2;
}

void TBitField::ClrBit(const int n) // очистить бит
{
	if ((n >= 0) && (n < BitLen))
		 pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] & (~GetMemMask(n));
	else
		 throw 2;
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	if ((n >= 0) && (n < BitLen))
		 return ((pMem[GetMemIndex(n)] & GetMemMask(n)) != 0);
	else
		 throw 2;
}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	if (&bf != this)
		 {
			delete[]pMem;
			MemLen = bf.MemLen;
			BitLen = bf.BitLen;
			pMem = new TELEM[MemLen];
			 if (pMem != nullptr)
			 {
					for (int i = 0; i < MemLen; i++)
						pMem[i] = bf.pMem[i];
					return (*this);
			 }
		  }
		  else
					throw 3;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	int res = 1;
	if (BitLen != bf.BitLen)
	{
		res = 0;
	}
	else 
		for (int i = 0; i < MemLen;i++)
			if (pMem[i] != bf.pMem[i])
			{ 
				res = 0; 
				break; 
			}
	return res;
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
	return !(*this == bf);
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	TBitField result((BitLen >= bf.GetLength()) ? *this : bf);
		 for (int i = 0; i < ((BitLen >= bf.GetLength()) ? bf.GetLength() : BitLen); i++)
			if (GetBit(i) | bf.GetBit(i))
					result.SetBit(i);
			else
					result.ClrBit(i);
	return result;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	TBitField result((BitLen >= bf.GetLength()) ? BitLen : bf.GetLength());
	for (int i = 0; i < ((BitLen >= bf.GetLength()) ? bf.GetLength() : BitLen); i++)
		 if (GetBit(i) & bf.GetBit(i))
				result.SetBit(i);
		 else
				result.ClrBit(i);
	return result;
}

TBitField TBitField::operator~(void) // отрицание
{
	TBitField temp(*this);
		for (int i = 0; i < BitLen; i++)
			temp.GetBit(i) ? temp.ClrBit(i) : temp.SetBit(i);
	return temp;
}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
	char value;
	for (int i = 0; i < bf.BitLen; i++)
		 {
			istr >> value;
			if (value == 0)
				bf.ClrBit(i);
			else if (value == 1)
				bf.SetBit(i);
				else
						break;
		 }
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	for (int i = 0; i < bf.BitLen; i++)
	{
		ostr << bf.GetBit(i);
	}
	return ostr;
}
