// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#include "tbitfield.h"
#define BIT_TELEM (sizeof(TELEM)*8)

TBitField::TBitField(int len)
{
	if (len < 0) throw 1; // "Error: negative length!"	
	BitLen = len;
	MemLen = (len / BIT_TELEM) + 1;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
		pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
		pMem[i] = bf.pMem[i];
}

TBitField::~TBitField()
{
	delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if (n < 0 || n >= BitLen) throw 2; // "Error: incorrect bit value"
	return (n / BIT_TELEM);
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	if (n < 0 || n >= BitLen) throw 2; // "Error: incorrect bit value"
	return (TELEM)(1 << (n % BIT_TELEM));
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
	return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	if (n < 0 || n >= BitLen) throw 2; // "Error: incorrect bit value"
	pMem[GetMemIndex(n)] |= GetMemMask(n);
}

void TBitField::ClrBit(const int n) // очистить бит
{
	if (n < 0 || n >= BitLen) throw 2; // "Error: incorrect bit value"
	pMem[GetMemIndex(n)] &= ~GetMemMask(n);
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	if (n < 0 || n >= BitLen) throw 2; // "Error: incorrect bit value"
	return (pMem[GetMemIndex(n)] & GetMemMask(n)) ? 1 : 0;
}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	if (*this != bf)
	{
		delete[] pMem;
		BitLen = bf.BitLen;
		MemLen = bf.MemLen;
		pMem = new TELEM[MemLen];
		for (int i = 0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];
	}

	return *this;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	TELEM value = 0;
	if (BitLen == bf.BitLen)
	{
		value = 1;
		for (int i = 0; i < bf.MemLen; i++)
			if (pMem[i] != bf.pMem[i]) { value = 0; break; }
	}
	return value;
}

int TBitField::operator !=(const TBitField &bf) const // сравнение
{
	return !(*this == bf);
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	int Len;
	(BitLen > bf.BitLen) ? Len = BitLen : Len = bf.BitLen;
	TBitField tmp(Len);
	for (int i = 0; i < tmp.MemLen; i++)
		tmp.pMem[i] = pMem[i] | bf.pMem[i];
	return tmp;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	int Len;
	(BitLen > bf.BitLen) ? Len = BitLen : Len = bf.BitLen;
	TBitField tmp(Len);
	for (int i = 0; i < tmp.MemLen; i++)
		tmp.pMem[i] = pMem[i] & bf.pMem[i];
	return tmp;
}

TBitField TBitField::operator~(void) // отрицание
{
	TBitField tmp(*this);
	for (int i = 0; i < BitLen; i++)
		(tmp.GetBit(i) == 0) ? tmp.SetBit(i) : tmp.ClrBit(i);

	return tmp;
}

// ввод/вывод

istream &operator >> (istream &istr, TBitField &bf) // ввод
{
	char value;
	for (int i = 0; i < bf.BitLen; i++)
	{
		istr >> value;
		if (value != '0' && value != '1') break;
		(value == '1') ? bf.SetBit(i) : bf.ClrBit(i);
	}

	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	for (int i = 0; i < bf.BitLen; i++)
		ostr << bf.GetBit(i);

	return ostr;
}